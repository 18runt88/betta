# Betta [![Build Status](https://travis-ci.org/dabapps/betta.svg?branch=master)](https://travis-ci.org/dabapps/betta)

### Simple Bootstrap Variable Editor - [www.betta.style](http://betta.style)

![Betta](static/img/press-screen.png)

### Development
You will need to install and run an http server. Example:

    npm install
    npm start


### Tests
    npm install
    npm test


### License
Copyright (c) 2015 Jake 'Sid' Smith, [DabApps](http://www.dabapps.com). For more information, see the LICENSE file in this repository.
